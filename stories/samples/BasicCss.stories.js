import { withLinks } from '@storybook/addon-links';
import { storiesOf } from '@storybook/html'; //testing
import { withRunScript } from 'storybook-addon-run-script/html'; //testing

import '../../themes/default/css/default.css';
import baseStyles from '../../themes/default/index.html';

// The following imported file will be imported as a string
import runScript from './BasicCss.runscript';

//Trials 2

storiesOf('Welcome/Samples', module)
  .addDecorator(withRunScript(runScript))
  .add('Basic', () => baseStyles);


//Trials 1

// storiesOf('Welcome/Samples', module)
//   .addDecorator(withLinks)
//   .add('Basic', () => baseStyles);


//Original codes

// export default {
//   title: 'Welcome/Samples',
//   decorators: [withLinks],
// };

// export const BasicStyles = () => baseStyles;
