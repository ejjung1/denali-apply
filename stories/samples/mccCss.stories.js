import { withLinks } from '@storybook/addon-links';

import '../../themes/mcc/css/theme.css';
import mccStyles from '../../themes/mcc/index.html';

export default {
  title: 'Welcome/Samples',
  decorators: [withLinks],
};

export const MCCStyles = () => mccStyles;
