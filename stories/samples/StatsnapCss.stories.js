import { withLinks } from '@storybook/addon-links';
import { storiesOf } from '@storybook/html'; //testing 2
import { withRunScript } from 'storybook-addon-run-script/html'; //testing 3

import '../../themes/statsnap/css/theme.css';
import statsnapStyles from '../../themes/statsnap/index.html';

// The following imported file will be imported as a string
import runScript from './StatsnapCss.runscript';

//Trial 3

storiesOf('Welcome/Samples', module)
  .addDecorator(withRunScript(runScript))
  .add('Statsnap', () => statsnapStyles);


//trial 2

// storiesOf('Welcome/Samples', module)
//   .addDecorator(withLinks)
//   .add('Statsnap', () => statsnapStyles);

//trial

// export default {
//   component: statsnapStyles,
//   title: 'Welcome/Samples',
//   decorators: 'statsnapStyles',
// };

// const Template = args = <statsnapStyles />;

// export const Default = Template.bind({});

// export const StatsnapStyles = () => statsnapStyles;



//original codes

// export default {
//   title: 'Welcome/Samples',
//   decorators: [statsnapStyles],
// };

// export const StatsnapStyles = () => statsnapStyles;