import './header.css';
import { createButton } from './Button';

export const createHeader = ({ user, onLogout, onLogin, onCreateAccount }) => {
  const header = document.createElement('header');

  const wrapper = document.createElement('div');
  wrapper.className = 'wrapper';

  const logo = `<div>
    <p>
      <svg id="Layer_3" data-name="Layer 3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 507.82 88.58" width="70%">
        <defs><style>.cls-1{fill:#243e7d;}</style></defs>
          <rect class="cls-1" y="0.12" width="19.66" height="88.46"/>
          <path class="cls-1" d="M134.32,92.15H71.45a1.9,1.9,0,0,1-1.9-1.91l-.11-52.71a1.91,1.91,0,0,1,1.91-1.9h54.9a1.9,1.9,0,0,1,1.9,1.9L128.26,49h20.65V35.47a15.68,15.68,0,0,0-15.69-15.69H65.5A15.68,15.68,0,0,0,49.81,35.47V92.55A15.69,15.69,0,0,0,65.5,108.24h83.41Z" transform="translate(-2.21 -19.66)"/>
          <path class="cls-1" d="M253.48,19.78H192.07l-20.74-.12v88.46l19.64.12V35.47h54.54a2.63,2.63,0,0,1,2.63,2.63V55.68a2.63,2.63,0,0,1-2.63,2.63H195.38L212.15,74.2h41.33a14.66,14.66,0,0,0,14.66-14.66V34.44A14.66,14.66,0,0,0,253.48,19.78Z" transform="translate(-2.21 -19.66)"/>
          <path class="cls-1" d="M368,54.39h-62.8A2.39,2.39,0,0,1,302.8,52V38a2.38,2.38,0,0,1,2.39-2.38h73.53l-15-15.85H299a14,14,0,0,0-14,14V57.1a14,14,0,0,0,14,14h62.8a2.38,2.38,0,0,1,2.38,2.38V89.76a2.39,2.39,0,0,1-2.38,2.39H288.23l15,15.85H368a14,14,0,0,0,14-14V68.35A14,14,0,0,0,368,54.39Z" transform="translate(-2.21 -19.66)"/>
          <path class="cls-1" d="M472.13,73.86h18.62A13.35,13.35,0,0,0,504.1,60.51V33.13a13.35,13.35,0,0,0-13.35-13.35h-84V108h19.64V35.63H480a3.94,3.94,0,0,1,3.95,3.94V56A4,4,0,0,1,480,60H431.44l51.77,48.25H510Z" transform="translate(-2.21 -19.66)"/>
      </svg>
    </p>
    <p>Inter-university Consortium for Political and Social Research</p>
  </div>`;

  wrapper.insertAdjacentHTML('afterbegin', logo);

  const account = document.createElement('div');
  if (user) {
    account.appendChild(createButton({ size: 'small', label: 'Log out', onClick: onLogout }));
  } else {
    account.appendChild(createButton({ size: 'small', label: 'Log in', onClick: onLogin }));
    account.appendChild(
      createButton({
        size: 'small',
        label: 'Sign up',
        onClick: onCreateAccount,
        primary: true,
      })
    );
  }
  wrapper.appendChild(account);
  header.appendChild(wrapper);

  return header;
};
