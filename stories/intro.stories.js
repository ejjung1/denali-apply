import { withLinks } from '@storybook/addon-links';

import './intro.css';
import welcome from './intro.html';

export default {
  title: 'Welcome/Intro',
  decorators: [withLinks],
};

export const Welcome = () => welcome;
