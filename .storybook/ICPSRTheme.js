import { create } from '@storybook/theming';
import ICPSRimg from './static/icpsr-logo.png';

export default create({
  base: 'light',

  colorPrimary: '#7F3E1B',
  colorSecondary: '#393E76',

  // UI
  appBg: '#f2f2f2',
  appContentBg: '#ffffff',
  appBorderColor: '#A3A3A3',
  appBorderRadius: 6,

  // Typography
  fontBase: '"PT Sans", sans-serif',
  fontCode: 'monospace',

  // Text colors
  textColor: '#222222',
  textInverseColor: 'rgba(255,255,255,0.9)',

  // Toolbar default and active colors
  barTextColor: '#606060',
  barSelectedColor: '#016EFF',
  barBg: 'white',

  // Form colors
  inputBg: '#f8f8f8',
  inputBorder: '#f8f8f8',
  inputTextColor: '#222222',
  inputBorderRadius: 5,

  brandTitle: 'Welcome to ICPSR Design System',
  brandUrl: '#',
  brandImage: ICPSRimg,
});