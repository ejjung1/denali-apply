import { addons } from '@storybook/addons';
import ICPSRTheme from './ICPSRTheme';

addons.setConfig({
  theme: ICPSRTheme,
});